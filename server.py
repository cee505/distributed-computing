#!/usr/bin/python

import cgi
import numpy
import pylab
import sqlite3 as DBI
#from job_manager import *

#Required to prevent internal server error
print("Content-Type: text/html\n")
form = cgi.FieldStorage()

# connect to the database
db = dbi.connect("database.db") ##our database
cursor = db.cursor()

#Inputs
############################################
UID = form.getvalue('UID')
JOB = form.getvalue('JOB')
RESULT = form.getvalue('RESULT')

#Input processing
received = dict(UID=UID,JOB=JOB,RESULT=RESULT) 
#print received

cursor.execute("SELECT count(answer) from result;")
count = cursor.fetchone()[0]
if count >= 10
	send = dict(JOB = 'stop')
	print(send)
else
	if received['JOB'] == 'new':
		#generate new random number set
		############################################
		#Pick v1-v5 from a normal distribution
		p = [-1,-1,-1,-1,-1]
		mu, sigma = 5, 1
		p[0] = float(numpy.random.normal(mu,sigma,1))
		mu, sigma = 2, 0.25
		p[1] = float(numpy.random.normal(mu,sigma,1))
		mu, sigma = -2, 0.75
		p[2] = float(numpy.random.normal(mu,sigma,1))
		mu, sigma = 0.5, 0.1
		p[3] = float(numpy.random.normal(mu,sigma,1))
		mu, sigma = 1.234, 0.321
		p[4] = float(numpy.random.normal(mu,sigma,1))

		valueset = dict(p1=p[0], p2=p[1], p3=p[2], p4=p[3], p5=p[4])
		#print valueset

		#Check whether this valueset is already in the database
		########################################################
		cursor.execute("SELECT COUNT(*) FROM parameter WHERE p1 = p[0] AND p2 = p[1] AND p3 = p[2] AND p4 = p[3] AND p5 = p[4]);")
		i = cursor.fetchone()[0]
		if i > 0
			#[draw 5 new numbers]

		#It's good, so we can send those numbers back now
		####################################################
		#append received dict with valueset
		send = dict(received, **valueset)
		print(send)

		#And write this new assignment to the database
		###########################################
		cursor.execute("SELECT MAX(id) FROM parameter;")
		newid = (cursor.fetchone()[0])+1
		cursor.execute("INSERT INTO parameter VALUES(newid,1,p[0],p[1],p[2],p[3],p[4]);")

	elif received['JOB'] == 'submit':
		#Note submitted values and result
		###########################################
		try:
			cursor.execute("INSERT INTO result (answer) VALUES(received['RESULT']);")
			print('TRUE')
		except:
			print('FALSE')
	elif:
		print('invalid job command')


#Data visualization
##############################################
#1. Get values from database
#pylab.hist(v, bins=50, normed=1)       # matplotlib version (plot)
#pylab.show()

'''
XINSHENG'S MAIN FUNCTION FROM job_manager
#############################################

def main():
    form = cgi.FieldStorage()
    manger=job_manager('./database.db')

    # set name or use default name if no name was provided
    if form.has_key("username"): 
        username = form["username"].value
    else:
        username = 'Empty'
    userid=manger.register(username)#if the username exist in databse, it will return user.id, 
    #or it will create a new user.id and return that
'''